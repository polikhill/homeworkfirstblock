//
//  ZeroExercise.swift
//  HomeworkFirstBlock
//
//  Created by Polina on 05.10.17.
//  Copyright © 2017 Polina. All rights reserved.
//

import UIKit

class ZeroExercise: NSObject {

    static func comparisonOfNumbers(firstNumber: Int, secondNumber: Int) {
        if firstNumber > secondNumber {
            print("\(firstNumber) greater than \(secondNumber)")
        }
        else {
            print("\(secondNumber) greater than \(firstNumber)")
        }
    }

}
